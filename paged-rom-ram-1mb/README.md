rc2014-compat
=============

A respin of the [RC2014](https://rc2014.co.uk/) ecosystem.  I don't have any
opposition to building things out of SMD parts (in some ways, I find it
easier to work with), so some boards might go that way to the extent that
parts availability allows.  Also, I'd like to keep all board sizes within
the 100x100mm limit within which PCB fabs offer their cheapest rates (like
$2 double-sided at JLCPCB, for instance).

paged-ram-rom-1mb
-----------------

This started out as a copy of the schematic from [the RC2014
GitHub](https://github.com/RC2014Z80/RC2014/tree/master/Hardware/512kROMRAM%20RC2014). 
I brought it into KiCad 6, replaced the "rescued" symbols from the original
with more current ones in the KiCad library, redid the connections in what I
think is an easier-to-follow style (global labels instead of buses), and
re-did the PCB layout from scratch (since that wasn't included).  I also
swapped out SMDs for through-hole parts.

This freed up enough space to add a second SRAM chip.  Banks 0-31 select ROM
(flash) and banks 32-95 (previously 32-63) select RAM.  16K chunks of either
ROM or RAM can be paged into any of four slots in the memory map:
$0000-$3FFF, $4000-$7FFF, $8000-$BFFF, $C000-$FFFF.  Reset maps ROM page 0
into all slots.
