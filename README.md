rc2014-compat
=============

A respin of the [RC2014](https://rc2014.co.uk/) ecosystem.  I don't have any
opposition to building things out of SMD parts (in some ways, I find it
easier to work with), so some boards might go that way to the extent that
parts availability allows.  Also, I'd like to keep all board sizes within
the 100x100mm limit within which PCB fabs offer their cheapest rates (like
$2 double-sided at JLCPCB, for instance).

Available boards will be listed below as I add them.  KiCad 6 is required to
work with the designs.  Make sure to check out the repo with ```git clone
--recursive ...``` to get the extra KiCad libraries some projects use.

rc2014-template
---------------

I took the board file from [here](https://rc2014.co.uk/1377/module-template/)
and fleshed it out into a full template for use with KiCad.  It supports
the extended signals (such as NMI, WAIT, and the second clock), but not
16-bit CPUs...see below for 16-bit support.

Boards built from this template will fit either backplane or backplane16
(see below) or any RC2014-compatible backplane.  Many backplanes have only
the base 39-pin connector; the 10-pin extension will be left flapping in the
breeze if you use one of those.  Some backplanes are 40 pins wide; make sure
you get pin 1 where it belongs, or bad things will happen.

rc2014-template16
-----------------

The above, but 16-bit.

Boards built from this template will fit backplane16 (see below) or any
extended RC2014-compatible backplane.  Again, watch out for 40-pin-wide
backplanes.

6502-cpu
--------

The Apple II got me through high school and college; writing software for it
was the first computer-related activity that made me money (small amounts in
retrospect, but it was a start).  I'm far more familiar with the 6502 than
the Z80, so it'd be nice to be able to work with one here.  

This board is a re-creation of [this
board](https://github.com/ancientcomputing/rc2014/tree/master/eagle/6502_board_revb),
recaptured in KiCad and laid out to fit within the template provided above. 
The Tindie page provides these resources, which I probably ought to include
in this repo somehow:

* [Monitor source](https://github.com/ancientcomputing/rc2014/tree/master/source/6502/monitor)
* [EhBASIC source](https://github.com/ancientcomputing/rc2014/tree/master/source/6502/ehbasic)
* [ROM images](https://github.com/ancientcomputing/rc2014/tree/master/rom/6502)
* [RC2014 compatibility notes](http://ancientcomputing.blogspot.com/2017/05/a-6502-cpu-for-rc2014-part-1.html)

A long-term goal would be to take the dialect of Microsoft BASIC Apple used
in the II and modify it to work here, perhaps with improved graphics from a
TMS9918 or one of its successors.  The Apple II's monitor would also be nice
to have, but while Apple open-sourced Integer BASIC and Microsoft
open-sourced its BASIC, I'm less sure about the Apple II monitor.  (The
Apple I monitor was open-sourced...that's the "WozMon" that's easily
located.)

One significant change from the original design: the I/O page can be changed
from the default $C0 if desired by cutting and soldering the 8 jumpers near
the 74LS688.  This happens to also be the page the Apple II uses for I/O, so
I'm inclined to leave it alone. :)

z80-cpu
-------

Basically the same as the RC2014 Z80 module, but with a 10-MHz LQFP-44 CPU
and a crystal oscillator to run it at that speed.

backplane
---------

A modular backplane that provides 4 slots with 20-mm spacing between them on
a 100x100mm double-sided PCB, with USB-C power input, a power-on reset
circuit (with reset button), a 4-pin TTL UART connector, and an idiot light
that indicates it's receiving power.  50-pin connectors at opposite edges
allow daisy-chaining more boards together.  Holes for M3 mounting screws are
provided.  If only the slots and daisy-chaining are needed, all the wiring
for those is on the bottom, so you could make those with single-sided PCBs
if you want.

The UART pin labeling for TX and RX is intentionally swapped (the RX line is
labeled "TX" and vice versa).  When plugging in your cable, connect TX to TX
and RX to RX.

backplane16
-----------

Same as the above, but with support for 16-bit CPUs (though the address bus
is still 16-bit too...OK for the TMS9900 with its 15-bit address bus, or
maybe you can repurpose some of the USR pins as extra address lines for an
8086 or 65816.  (Both of those used the data pins to carry some of the
address lines, which you'd probably need to sort out in your CPU board
design.)

The second TTL UART port is also brought out to a connector.  64-pin
connectors are used for daisy-chaining.  An adapter to connect 8- and 16-bit
backplanes together is left as an exercise for the reader.

backplane16pcie
---------------

Who knew right-angle headers and matching sockets would be so hard to come
by?  This is a respin of backplane16 that replaces difficult-to-source (and
expensive when you find them) connectors at each end with the connectors
your newer computer uses for its expansion slots.  A 4-lane PCI Express
connector has 64 connections inside...perfect for our needs here. 
Straddle-mount sockets for one end are fairly cheap (under $4 from a
domestic distributor), and since the mating connector at the other end is
integrated into the PCB layout, it's basically free.

v9958-video
-----------

A video card built around the Yamaha V9958, a more powerful successor to the
TI TMS9918A with support for 80-column text, up to 512x384 graphics
resolution, and up to 192K of dedicated memory that can be installed in 64K
chunks.  (As cheap as RAM is now, you might as well max it out.)

I/O configuration is fairly flexible.  You can allocate either two addresses
to it (like the 9918A, which will restrict access to the 9958's added
features) or four (for access to all features), depending on your needs. 
Four addresses at $98-$9B is the MSX-compatible configuration that I've seen
another 9958 board use, while two addresses at $BE-$BF might run well-behaved
ColecoVision games.  Want a multi-monitor setup?  Install multiple cards,
all on different addresses.

68b50-dual-serial
-----------------

Two serial ports, one card.  One is connected to the TX/RX lines of the
backplane for a TTL-level console port, while the other drives a 9-pin
serial port through a MAX232 to control printers, modems, and other RS-232
devices.  Bitrates of 115.2, 38.4, 19.2, and 9.6 kbps are independently
selectable with jumpers, and by using the 6850's internal bitrate divider,
rates down to 300 bps can be set up if needed.  The card's base I/O address
is also jumper-selectable.

This board has its own 7.3728-MHz crystal oscillator, making it independent
of CPU speed.  (I'm aiming for 10 MHz with both of my CPU boards.)

paged-ram-rom
-------------

This started out as a copy of the schematic from [the RC2014
GitHub](https://github.com/RC2014Z80/RC2014/tree/master/Hardware/512kROMRAM%20RC2014). 
I brought it into KiCad 6, replaced the "rescued" symbols from the original
with more current ones in the KiCad library, redid the connections in what I
think is an easier-to-follow style (global labels instead of buses), and
re-did the PCB layout from scratch (since that wasn't included).  I also
swapped out SMDs for through-hole parts.

I was having some trouble puzzling out how the I/O decoding worked in the
original schematic.  I found [this page](https://bread80.com/2020/06/27/understanding-the-rc2014-512k-rom-512k-ram-board/)
that described how the board worked, and once I had redone the schematic
with the newer symbols, it became clear what was going on.  In particular,
the newer symbols for the 74LS139 have the selection pins labeled A1 and A0
instead of A and B.  Looking back now at the function table on page 1 of the
[TI datasheet](https://www.ti.com/lit/ds/symlink/sn74s139a.pdf?HQS=dis-dk-null-digikeymode-dsf-pf-null-wwe&ts=1658471109258&ref_url=https%253A%252F%252Fwww.ti.com%252Fgeneral%252Fdocs%252Fsuppproductinfo.tsp%253FdistId%253D10%2526gotoUrl%253Dhttps%253A%252F%252Fwww.ti.com%252Flit%252Fgpn%252Fsn74s139a),
I see that the "select" column headings are in reverse alphabetical order
(B, then A).  That must've escaped me while I was trying to puzzle out the
schematic.

paged-ram-rom-1mb
-----------------

Converting the previous design to SMD freed up enough space to add a second
SRAM chip for a total of 1 MB.  Flash is now PLCC instead of DIP: smaller,
but still socketed.

new-board.sh
------------

Instead of setting up the templates within KiCad, you can add new boards
within this project with this script.  The first parameter is the name of
the board; the second (optional) parameter is "16" to use the 16-bit
template (otherwise, use the 8-bit template).
