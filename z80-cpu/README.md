rc2014-compat
=============

A respin of the [RC2014](https://rc2014.co.uk/) ecosystem.  I don't have any
opposition to building things out of SMD parts (in some ways, I find it
easier to work with), so some boards might go that way to the extent that
parts availability allows.  Also, I'd like to keep all board sizes within
the 100x100mm limit within which PCB fabs offer their cheapest rates (like
$2 double-sided at JLCPCB, for instance).

z80-cpu
-------

Basically the same as the RC2014 Z80 module, but with a 10-MHz LQFP-44 CPU
and a crystal oscillator to run it at that speed.

