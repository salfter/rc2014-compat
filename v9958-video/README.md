rc2014-compat
=============

A respin of the [RC2014](https://rc2014.co.uk/) ecosystem.  I don't have any
opposition to building things out of SMD parts (in some ways, I find it
easier to work with), so some boards might go that way to the extent that
parts availability allows.  Also, I'd like to keep all board sizes within
the 100x100mm limit within which PCB fabs offer their cheapest rates (like
$2 double-sided at JLCPCB, for instance).

v9958-video
-----------

A video card built around the Yamaha V9958, a more powerful successor to the
TI TMS9918A (introduced in the TI-99/4A, if that gives you an idea how far
back it goes) with support for 80-column text, up to 512x384 graphics
resolution, and up to 192K of dedicated memory that can be installed in 64K
chunks.  (As cheap as RAM is now, you might as well max it out.)

I/O configuration is fairly flexible.  You can allocate either two addresses
to it (like the 9918A, which will restrict access to the 9958's added
features) or four (for access to all features), depending on your needs. 
Four addresses at $98-$9B is the MSX-compatible configuration that I've seen
another 9958 board use, while two addresses at $BE-$BF might run well-behaved
ColecoVision games.  Want a multi-monitor setup?  Install multiple cards,
all on different addresses.

