rc2014-compat
=============

A respin of the [RC2014](https://rc2014.co.uk/) ecosystem.  I don't have any
opposition to building things out of SMD parts (in some ways, I find it
easier to work with), so some boards might go that way to the extent that
parts availability allows.  Also, I'd like to keep all board sizes within
the 100x100mm limit within which PCB fabs offer their cheapest rates (like
$2 double-sided at JLCPCB, for instance).

backplane16pcie
---------------

Who knew right-angle headers and matching sockets would be so hard to come
by?  This is a respin of backplane16 that replaces difficult-to-source (and
expensive when you find them) connectors at each end with the connectors
your newer computer uses for its expansion slots.  A 4-lane PCI Express
connector has 64 connections inside...perfect for our needs here. 
Straddle-mount sockets for one end are fairly cheap (under $4 from a
domestic distributor), and since the mating connector at the other end is
integrated into the PCB layout, it's basically free.

Beyond that, the board provides support for both extended (second row of 10
pins) and 16-bit (second row of 23 pins) RC2014-compatible cards.  It breaks
out both UART connections to 4-pin connectors (note: TX and RX labels are
intentionally swapped; connect TX to TX and RX to RX), uses any USB-C power
supply, and provides power-on reset and a reset button.