#!/usr/bin/env bash
for i in */fabrication
do 
  (cd $i; rm ${i%/fabrication}.zip; zip -9 ${i%/fabrication}.zip *.gbr *.drl)
done
