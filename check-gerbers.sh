#!/usr/bin/env bash

# see if gerbers need updating before commit

for i in `find -type d -name fabrication | sed "s/\/fabrication//;s/.\///"`
do 
  if [ $i/$i.kicad_pcb -nt $i/fabrication/$i-F_Cu.gbr ]
  then 
    echo $i: update gerbers
  fi
done
