rc2014-compat
=============

A respin of the [RC2014](https://rc2014.co.uk/) ecosystem.  I don't have any
opposition to building things out of SMD parts (in some ways, I find it
easier to work with), so some boards might go that way to the extent that
parts availability allows.  Also, I'd like to keep all board sizes within
the 100x100mm limit within which PCB fabs offer their cheapest rates (like
$2 double-sided at JLCPCB, for instance).

68b50-dual-serial
-----------------

Two serial ports, one card.  One is connected to the TX/RX lines of the
backplane for a TTL-level console port, while the other drives a 9-pin
serial port through a MAX232 to control printers, modems, and other RS-232
devices.  Bitrates of 115.2, 38.4, 19.2, and 9.6 kbps are independently
selectable with jumpers, and by using the 6850's internal bitrate divider,
rates down to 300 bps can be set up if needed.  The card's base I/O address
is also jumper-selectable.

This board has its own 7.3728-MHz crystal oscillator, making it independent
of CPU speed.  (I'm aiming for 10 MHz with both of my CPU boards.)

